# MAC0110 - MiniEP7
# Maximilian C Göritz - 11795418

function taylor(par, x)
	x %= 2*pi

	if par
		exp = 0
	else
		exp = 1
	end

	resul = x^exp
	last = resul
	exp += 2
	while exp <= 21
		last = (last*(x^2)/(exp*(exp-1)))
		resul += ((-1) ^ (exp ÷ 2))*last
		exp += 2
	end

	return resul
end

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

function factorial(x)
	resul = x
	for i in 1:(x-1)
		resul *= i
	end
	return resul
end

function sin_(x)
	return taylor(false, x)
end

function cos_(x)
	return taylor(true, x)
end

function tan_(x)

	resul = BigInt(0)

	x = x % pi

	if x < 0
		x = -x
		neg = true
	else
		neg = false
	end

	if x == 0.5*pi
		return Inf
	end

	if x > 0.5*pi
		x -= pi
	end

	for i = 1:20
		expo1 = big(2^(2*i))
		expo2 = big(2^(2*i)-1)
		expo3 = big(x^(2*i-1))
		parte = big(expo1*expo2*bernoulli(i)*expo3)/factorial(2i)
		resul = big(resul + parte)
	end

	if neg
		return -resul
	else
		return resul
	end
end

function quaseigual(x, y)
	return abs(x-y) <= 0.001
end

function check_sin(value, x)
	return quaseigual(sin_(x), value)
end

function check_cos(value, x)
	return quaseigual(cos_(x), value)
end

function check_tan(value, x)
	return quaseigual(tan(x), value)
end

function taylor_sin(x)
	return taylor(false, x)
end

function taylor_cos(x)
	return taylor(true, x)
end

function taylor_tan(x)

	resul = BigInt(0)

	x = x % pi

	if x < 0
		x = -x
		neg = true
	else
		neg = false
	end

	if x == 0.5*pi
		return Inf
	end

	if x > 0.5*pi
		x -= pi
	end

	for i = 1:20
		expo1 = big(2^(2*i))
		expo2 = big(2^(2*i)-1)
		expo3 = big(x^(2*i-1))
		parte = big(expo1*expo2*bernoulli(i)*expo3)/factorial(2i)
		resul = big(resul + parte)
	end

	if neg
		return -resul
	else
		return resul
	end
end

function test()
	inps  = [1/3, 1/4, 1/6, 0, 1/2]
	for i in inps
		if !check_sin(sin(i*pi), i*pi)
			println("Erro em sin ", i, " pi")
			println("sin ($i pi) = ", sin(i*pi))
			println("sin_($i pi) = ", sin_(i*pi))
			return
		end
		if !check_cos(cos(i*pi), i*pi)
			println("Erro em cos ", i, " pi")
			println("cos ($i pi) = ", cos(i*pi))
			println("cos_($i pi) = ", cos_(i*pi))
			return
		end
		if (i)%0.5 != 0 && !check_tan(tan(i*pi), i*pi)
			println("Erro em tan ", i, " pi")
			println("tan ($i pi) = ", tan(i*pi))
			println("tan_($i pi) = ", tan_(i*pi))
			return
		end
	end
	println("Passou!!")
end

test()
